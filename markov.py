import random

def generate(filename: str, start_words: list[str], chain_length: int, num_generated: int) -> str:
   
    with open(filename, 'r', encoding='utf-8') as file:
        text = file.read()

    words = text.split()
    markov_chains = {}
    for i in range(len(words) - chain_length):
        key = tuple(words[i:i + chain_length])
        value = words[i + chain_length]
        if key in markov_chains:
            markov_chains[key].append(value)
        else:
            markov_chains[key] = [value]


    generated_words = start_words.copy()
    while len(generated_words) < num_generated:
        current_key = tuple(generated_words[-chain_length:])
        if current_key in markov_chains:
            next_word = random.choice(markov_chains[current_key])
            generated_words.append(next_word)
        else:
            break

    generated_sentence = ' '.join(generated_words)

    return generated_sentence

filename = 'Test.txt' 
start_words = ['The', 'quick', 'brown'] 
chain_length = 2 
num_generated = 20 
generated_text = generate(filename, start_words, chain_length, num_generated)
print(generated_text)