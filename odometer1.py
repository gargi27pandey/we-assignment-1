

def is_valid_reading(reading):
    
    reading_str = str(reading)
    if '0' in reading_str:
        return False  # Reading cannot contain 0
    return all(reading_str[i] < reading_str[i+1] for i in range(len(reading_str) - 1))



def next_reading(reading):
    
    reading_str = str(reading)
    if '0' in reading_str:
        print("Invalid reading provided.")
        return

    for next_reading in range(reading + 1, 1000):
        next_reading_str = str(next_reading)
        if all(next_reading_str[i] > next_reading_str[i-1] for i in range(1, len(next_reading_str))):
            return next_reading
        if '0' in next_reading_str or '1' in next_reading_str:
            continue

    # If we reach here, it means we need to handle the case where the next valid reading requires increasing the number of digits
    num_digits = len(reading_str)
    return int(''.join([str(i) for i in range(1, num_digits + 1)]))


def nth_reading_before(reading, n):
    
    if not is_valid_reading(reading):
        print("Invalid reading provided.")
        return

    num_digits = len(str(reading))
    max_reading = int(''.join([str(9 - num_digits + i) for i in range(1, num_digits + 1)]))

    if reading == max_reading:
        smallest_reading = int(''.join([str(i) for i in range(1, num_digits + 1)]))
        result = smallest_reading
        for _ in range(n):
            while not is_valid_reading(result):
                result -= 1
        return result

    result = reading
    for _ in range(n):
        result -= 1
        while not is_valid_reading(result):
            result -= 1
            if result < 100:
                print("Invalid reading provided.")
                return

    return result


def calculate_distance_between_readings(reading1, reading2):
    
    if not is_valid_reading(reading1) or not is_valid_reading(reading2):
        print("Invalid readings provided.")
        return -1

    num_digits1 = len(str(reading1))
    num_digits2 = len(str(reading2))

    if num_digits1 != num_digits2:
        print("Readings have different number of digits.")
        return -1

    if reading1 < reading2:
        return reading2 - reading1
    else:
        max_reading = int(''.join([str(9 - num_digits1 + i) for i in range(1, num_digits1 + 1)]))
        return max_reading - reading1 + reading2 - 123 + 1

is_valid_reading(890)

print(next_reading(789))
print(prev_reading(789))



nth_reading_after(6989,3)

print(nth_reading_before(145,5))

print(calculate_distance_between_readings(789, 123))