import random

def roll_dice(num_dice):
    
    return [random.randint(1, 6) for _ in range(num_dice)]

def display_dice(dice):
    
    print("Dice:", dice)

def choose_dice_to_keep():
   
    choices = input("Enter the indexes of the dice to keep (e.g., 1 3 5): ").split()
    return [int(choice) - 1 for choice in choices]

def reroll_dice(dice, keep_indices):
   
    for i in range(len(dice)):
        if i not in keep_indices:
            dice[i] = random.randint(1, 6)
    return dice

def get_category_choice():
    
    return int(input("Choose a scoring category (1-13): "))

def score_dice(dice, category):
    
    if category == 1:
        return sum([die for die in dice if die == 1])
    elif category == 2:
        return sum([die for die in dice if die == 2])
    else:
        return 0

def main():
    print("Welcome to Yahtzee!")
    scorecard = [0] * 13
    rolls_left = 3

    while any(score == 0 for score in scorecard):
        print("\nRound Start:")
        dice = roll_dice(5)
        display_dice(dice)

        while rolls_left > 0:
            print("\nRolls Left:", rolls_left)
            keep_indices = choose_dice_to_keep()
            dice = reroll_dice(dice, keep_indices)
            display_dice(dice)
            rolls_left -= 1

        category = get_category_choice()
        if scorecard[category - 1] == 0:
            score = score_dice(dice, category)
            scorecard[category - 1] = score
            print("Scored", score, "points for category", category)
        else:
            print("Category already scored.")

        rolls_left = 3

    print("\nGame Over!")
    print("Final Score:", sum(scorecard))

if __name__ == "__main__":
    main()