import random

class CardGame:
    def __init__(self, player_name):
        self.player_name = player_name
        self.card_values = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14}
        self.player_scores = {player_name: 0, 'Computer': 0}
        self.computer_hand = list(self.card_values.keys())

    def play(self):
        diamond_cards = list(self.card_values.keys())
        diamond_cards.remove('A')
        diamond_cards = [card for card in diamond_cards if card != 'D']
        random.shuffle(diamond_cards)

        for diamond_card in diamond_cards:
            user_bid = self.get_bid(self.player_name)
            computer_bid = self.get_computer_bid()
            if user_bid > computer_bid:
                self.player_scores[self.player_name] += len(diamond_card)
            elif computer_bid > user_bid:
                self.player_scores['Computer'] += len(diamond_card)
            else:
                self.player_scores[self.player_name] += len(diamond_card) / 2
                self.player_scores['Computer'] += len(diamond_card) / 2

    def get_bid(self, player):
        print(f"{player}, your hand: {' '.join(self.card_values.keys())}")
        while True:
            bid_card = input("Choose a card to bid (2-9, T, J, Q, K): ").upper()
            if bid_card in self.card_values and bid_card != 'A' and bid_card != 'D':
                return self.card_values[bid_card]
            else:
                print("Invalid card. Please choose a valid card to bid.")

    def get_computer_bid(self):
        hand_values = [self.card_values[card] for card in self.computer_hand]
        max_value = max(hand_values)
        min_value = min(hand_values)

        if max_value > 10:
            bid_value = max_value
        else:
            bid_value = min_value
        self.computer_hand.remove(next(card for card, value in self.card_values.items() if value == bid_value))

        return bid_value

    def display_scores(self):
        print("Scores:")
        for player, score in sorted(self.player_scores.items(), key=lambda x: x[1], reverse=True):
            print(f"{player}: {score}")

if __name__ == "__main__":
    player_name = input("Enter your name: ")
    game = CardGame(player_name)
    print("\nStarting the game...\n")
    game.play()
    print("\nGame over. Displaying final scores:\n")
    game.display_scores()

import random

class CardGame:
    def __init__(self, player_name):
        self.player_name = player_name
        self.card_values = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14}
        self.player_scores = {player_name: 0, 'Computer': 0}
        self.computer_hand = list(self.card_values.keys())

    def play(self):
        diamond_cards = list(self.card_values.keys())
        diamond_cards.remove('A')
        diamond_cards = [card for card in diamond_cards if card != 'D']
        random.shuffle(diamond_cards)

        for diamond_card in diamond_cards:
            user_bid = self.get_bid(self.player_name)
            computer_bid = self.get_computer_bid()
            if user_bid > computer_bid:
                self.player_scores[self.player_name] += len(diamond_card)
            elif computer_bid > user_bid:
                self.player_scores['Computer'] += len(diamond_card)
            else:
                self.player_scores[self.player_name] += len(diamond_card) / 2
                self.player_scores['Computer'] += len(diamond_card) / 2

    def get_bid(self, player):
        print(f"{player}, your hand: {' '.join(self.card_values.keys())}")
        while True:
            bid_card = input("Choose a card to bid (2-9, T, J, Q, K): ").upper()
            if bid_card in self.card_values and bid_card != 'A' and bid_card != 'D':
                return self.card_values[bid_card]
            else:
                print("Invalid card. Please choose a valid card to bid.")

    def get_computer_bid(self):

        bid_probabilities = {}
        for card in self.computer_hand:
            bid_value = self.card_values[card]
            num_cards_higher = sum(1 for c in self.computer_hand if self.card_values[c] > bid_value)
            probability_win = num_cards_higher / len(self.computer_hand)
            bid_probabilities[bid_value] = probability_win

        expected_scores = {value: probability * len(card) for value, probability in bid_probabilities.items()}
        optimal_bid_value = max(expected_scores, key = expected_scores.get)

        self.computer_hand.remove(next(card for card, value in self.card_values.items() if value == optimal_bid_value))

        return optimal_bid_value

    def display_scores(self):
        print("Scores:")
        for player, score in sorted(self.player_scores.items(), key=lambda x: x[1], reverse=True):
            print(f"{player}: {score}")

if __name__ == "__main__":
    player_name = input("Enter your name: ")
    game = CardGame(player_name)
    print("\nStarting the game...\n")
    game.play()
    print("\nGame over. Displaying final scores:\n")
    game.display_scores()
4